//
//  Barcode.swift
//  Armood
//
//  Created by Orkhan Bayramli on 10/25/19.
//  Copyright © 2019 Orkhan Bayramli. All rights reserved.
//

import UIKit
import BarcodeScanner

class Barcode: BarcodeScannerViewController, BarcodeScannerCodeDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    func scanner(_ controller: BarcodeScannerViewController, didCaptureCode code: String, type: String) {
        print("captured")
    }

}
