//
//  TestViewController.swift
//  Armood
//
//  Created by Orkhan Bayramli on 10/24/19.
//  Copyright © 2019 Orkhan Bayramli. All rights reserved.
//

import UIKit
import Alamofire

class TestViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    
    let destination: DownloadRequest.DownloadFileDestination = { _, _ in
        
        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory,
                                                                .userDomainMask, true)[0]
        let documentsURL = URL(fileURLWithPath: documentsPath, isDirectory: true)
        let fileURL = documentsURL.appendingPathComponent("image.png")
        
        return (fileURL, [.removePreviousFile, .createIntermediateDirectories])
        
    }
    
    let imageUrl = "https://res.cloudinary.com/armood/image/upload/v1569259456/hd/WhatsApp_Image_2019-09-23_at_2.32.54_PM_rn7buq.jpg"
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        Alamofire.download(imageUrl, to: destination).responseData { (response) in
            print(response)
            if let image = response.value {
                self.imageView.image = UIImage(data: image)
            }
        }
    }
    
    

}
