//
//  ViewController.swift
//  Armood
//
//  Created by Orkhan Bayramli on 10/22/19.
//  Copyright © 2019 Orkhan Bayramli. All rights reserved.
//

import UIKit
import SceneKit
import ARKit
import BarcodeScanner
import Alamofire

class ViewController: UIViewController, ARSCNViewDelegate {

    @IBOutlet var sceneView: ARSCNView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set the view's delegate
        sceneView.delegate = self
        
        // Show statistics such as fps and timing information
        sceneView.showsStatistics = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Create a session configuration
        let configuration = ARImageTrackingConfiguration()
        
        // We are defining which images should be tracked giving its folder name and location
        if let trackedImages = ARReferenceImage.referenceImages(inGroupNamed: "AlbumImages", bundle: Bundle.main) {
            
            // We assign the images which will be tracked to the configuration
            configuration.trackingImages = trackedImages
            
            // Number of images that will be tracked simultaneously
            configuration.maximumNumberOfTrackedImages = trackedImages.count            
        }

        // Run the view's session
        sceneView.session.run(configuration)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Pause the view's session
        sceneView.session.pause()
    }

    // MARK: - ARSCNViewDelegate
    
    // Override to create and configure nodes for anchors added to the view's session.
    func renderer(_ renderer: SCNSceneRenderer, nodeFor anchor: ARAnchor) -> SCNNode? {
        let node = SCNNode()
     
        // If anchor is the type of ARImageAnchor then proceed.
        if let imageAnchor = anchor as? ARImageAnchor {
            
            guard let pairName = imageAnchor.referenceImage.name else {
                fatalError("Error occurred while detecting image!")
            }
            
            // Full name of the file including extension
            let videoNode = SKVideoNode(fileNamed: "\(pairName).mp4")
            videoNode.play()
            
            // 480 and 360 is because we are using 360p video.
            // An SKScene object represents a scene of content in SpriteKit.
            let videoScene = SKScene(size: CGSize(width: 480, height: 360))
            
            videoNode.position = CGPoint(x: videoScene.size.width / 2, y: videoScene.size.height / 2)
            
            videoNode.yScale = -1.0
            
            videoScene.addChild(videoNode)
            
            let plane = SCNPlane(width: imageAnchor.referenceImage.physicalSize.width, height: imageAnchor.referenceImage.physicalSize.height)
            
            // IMPORTANT. videoScene will be displayed on top of our image.
            plane.firstMaterial?.diffuse.contents = videoScene
            
            let planeNode = SCNNode(geometry: plane)
            planeNode.eulerAngles.x = -.pi / 2
            
            node.addChildNode(planeNode)
        }
        
        return node
    }

    @IBAction func barcodeButtonPressed(_ sender: UIBarButtonItem) {
        let barcodeViewController = makeBarcodeScannerViewController()
        barcodeViewController.title = "Barcode Scanner"
        barcodeViewController.messageViewController.messages.processingText = "Successfully read!"
        
        navigationController?.pushViewController(barcodeViewController, animated: true)
    }
    
    func makeBarcodeScannerViewController() -> BarcodeScannerViewController {
      let barcodeViewController = BarcodeScannerViewController()
      barcodeViewController.codeDelegate = self
      barcodeViewController.errorDelegate = self
      barcodeViewController.dismissalDelegate = self
      return barcodeViewController
    }
}


// MARK: - BarcodeScannerCodeDelegate
extension ViewController: BarcodeScannerCodeDelegate {
    func scanner(_ controller: BarcodeScannerViewController, didCaptureCode code: String, type: String) {
        print("Barcode Data: \(code)")
        print("Symbology Type: \(type)")
        
        UserDefaults.standard.set(code, forKey: "barcode")
        
                
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
            controller.navigationController?.popViewController(animated: true)
        }
    }
}

// MARK: - BarcodeScannerErrorDelegate
extension ViewController: BarcodeScannerErrorDelegate {
    func scanner(_ controller: BarcodeScannerViewController, didReceiveError error: Error) {
        print(error)
    }
}

// MARK: - BarcodeScannerDismissalDelegate
extension ViewController: BarcodeScannerDismissalDelegate {
    func scannerDidDismiss(_ controller: BarcodeScannerViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
}
